<?php
/*
 * Joomla JoomShopping
 * Модуль для подключения системы IntellectMoney
 *
  Last Changed Date: 2019-01-15 09:00:00
 */
?>
<?php
defined('_JEXEC') or die('Restricted access');

class pm_im extends PaymentRoot {

    function showPaymentForm($params, $pmconfigs) {
        include(dirname(__FILE__) . "/paymentform.php");
    }

    function showAdminFormParams($params) {
        $array_params = array('transaction_end_status', 'transaction_pending_status', 'transaction_failed_status');
        foreach ($array_params as $key) {
            if (!isset($params[$key]))
                $params[$key] = '';
        }
        $orders = &JModelLegacy::getInstance('orders', 'JshoppingModel'); //admin model
        include(dirname(__FILE__) . "/adminparamsform.php");
    }

    function checkTransaction($pmconfigs, $order, $act) {
        $secretKeyLocal = $pmconfigs['secret_key'];
        $eshopIdLocal = $pmconfigs['eshop_id'];
        $recipientAmountLocal = $this->convertAmount($order->order_total);
        $recipientCurrencyLocal = $pmconfigs['is_test'] ? 'TST' : $order->currency_code_iso;

        $eshopId = trim(stripslashes($_REQUEST['eshopId']));
        $orderId = trim(stripslashes($_REQUEST['orderId']));
        $serviceName = trim(stripslashes($_REQUEST['serviceName']));
        $eshopAccount = trim(stripslashes($_REQUEST['eshopAccount']));
        $recipientAmount = trim(stripslashes($_REQUEST['recipientAmount']));
        $recipientCurrency = trim(stripslashes($_REQUEST['recipientCurrency']));
        $paymentStatus = trim(stripslashes($_REQUEST['paymentStatus']));
        $userName = trim(stripslashes($_REQUEST['userName']));
        $userEmail = trim(stripslashes($_REQUEST['userEmail']));
        $paymentData = trim(stripslashes($_REQUEST['paymentData']));
        $secretKey = trim(stripslashes($_REQUEST['secretKey']));
        $hash = trim(stripslashes($_REQUEST['hash']));

        if ($recipientAmount != $recipientAmountLocal && $paymentStatus != '7')
            return array(0, "Ошибка: Сумма транзакции не совпадает с суммой заказа!");
        if ($recipientCurrency != $recipientCurrencyLocal)
            return array(0, "Ошибка: Код валюты транзакции не совпадает с кодом валюты заказа!");
        if ($eshopId != $eshopIdLocal)
            return array(0, "Ошибка: Номера сайта не соответствует!");

        if (isset($secretKey) && $secretKey) {
            if ($secretKey != $secretKeyLocal)
                return array(0, "Ошибка: Секретное слово не верное! SecretKey: " . $secretKey);
        }
        else {

            $control_hash = $eshopId . "::" . $orderId . "::" . $serviceName . "::" . $eshopAccount . "::" .
                    $recipientAmountLocal . "::" . $recipientCurrency . "::" . $paymentStatus . "::" . $userName . '::' .
                    $userEmail . '::' . $paymentData . '::' . $secretKeyLocal;

            $control_hash_urldecode = urldecode($control_hash);

            $control_hash_urldecode_utf8 = mb_convert_encoding($control_hash_urldecode, 'windows-1251', 'utf-8');

            $control_hash_urldecode_win = mb_convert_encoding($control_hash_urldecode, 'utf-8', 'windows-1251');

            switch ($hash) {
                case md5($control_hash):
                    break;
                case md5($control_hash_urldecode):
                    break;
                case md5($control_hash_urldecode_utf8):
                    break;
                case md5($control_hash_urldecode_win):
                    break;
                default:
                    return array(0, "Ошибка: Хеш не соответствует.");
            }
        }

        echo "OK";
        $statuses = array(
            "2" => 0,
            "3" => 1,
            "6" => 2,
            "7" => 3,
            "5" => 4,
            "8" => 5,
            "4" => 6
        );
        $lastIMStatus = $this->gesLastIMTransactionPaymentStatusData($order);

        if ($lastIMStatus === false || $statuses[$lastIMStatus->value] < $statuses[$paymentStatus]) {
            $transactionData = array("statusIM" => $paymentStatus);
            return array($paymentStatus, "Order ID :" . $order->order_id. ". Change status to ". $paymentStatus, $paymentId, $transactionData);
        }
    }

    function showEndForm($pmconfigs, $order) {
        include 'IntellectMoneyHelper/MerchantReceiptHelper.php';

        $notify_url = JURI::root() . "index.php?option=com_jshopping&controller=checkout&task=step7&act=notify&js_paymentclass=pm_im&no_lang=1";
        $return = JURI::root() . "index.php?option=com_jshopping&controller=checkout&task=step7&act=return&js_paymentclass=pm_im";
        $cancel_return = JURI::root() . "index.php?option=com_jshopping&controller=checkout&task=step7&act=cancel&js_paymentclass=pm_im";

        $host = 'https://merchant.intellectmoney.ru/ru/';
        $item_name = sprintf(_JSHOP_PAYMENT_NUMBER, $order->order_number);

        $merchantReceipt = $this->generateMerchantReceipt($pmconfigs, $order);
        $recipient_currency = $pmconfigs['is_test'] ? 'TST' : $order->currency_code_iso;
        $hold_mode = $pmconfigs['is_hold_mode'];
        $recipient_amount = $this->convertAmount($order->order_total);

        $pre_hold_time = (intval($pmconfigs['hold_time']) && $pmconfigs['hold_time'] > 0 && $pmconfigs['hold_time'] < 119) ? $pmconfigs['hold_time'] : 72;
        $hold_time = $hold_mode ? $pre_hold_time : null;

        $pre_expire_date = intval($pmconfigs['expire_date']) && $pmconfigs['expire_date'] > 0 && $pmconfigs['expire_date'] < 4319 ? $pmconfigs['expire_date'] : 4319;
        $expire_date = date('Y-m-d H:i:s', strtotime('+' . $pre_expire_date . ' hour'));

        $pre_hash = md5(join('::', array($pmconfigs['eshop_id'], "asoeuha u " . $order->order_id, $item_name, $recipient_amount, $recipient_currency, $pmconfigs['secret_key'])));
        ?>
        <html>
            <body>
            <head>
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />            
            </head>
            <form id="paymentform" action="<?php echo $host ?>" name="paymentform" method="post">
                <input type="hidden" name="eshopId" value="<?php echo $pmconfigs['eshop_id'] ?>">
                <input type="hidden" name="orderId" value="<?php echo $order->order_id ?>">
                <input type="hidden" name="serviceName" value="<?php echo $item_name ?>">
                <input type="hidden" name="recipientAmount" value="<?php echo $recipient_amount ?>">
                <input type="hidden" name="recipientCurrency" value="<?php echo $recipient_currency ?>">
                <input type="hidden" name="successUrl" value="<?php echo $return ?>">
                <input type="hidden" name="holdMode" value="<?php echo $hold_mode ?>">
                <input type="hidden" name="merchantReceipt" value="<?php echo $merchantReceipt ?>">
                <input type="hidden" name="preference" value="<?php echo $pmconfigs['preference']; ?>">
                <?php if (!empty($hold_time)) { ?>
                    <input type="hidden" name="holdTime" value="<?php echo $hold_time; ?>">
                <?php } ?>
                <input type="hidden" name="expireDate" value="<?php echo $expire_date; ?>">
                <input type="hidden" name="hash" value="<?php echo $pre_hash; ?>">
            </form>        
            <?php print _JSHOP_REDIRECT_TO_PAYMENT_PAGE ?>
            <br>
            <script type="text/javascript">document.getElementById('paymentform').submit();</script>
        </body>
        </html>
        <?php
        die();
    }

    private function generateMerchantReceipt($config, $order) {
        $merchant_helper = new PaySystem\MerchantReceiptHelper($order->order_total, null, $order->email, $pmconfigs['group']);
        $products = $order->getAllItems();
        foreach ($products as $product) {
            $merchant_helper->addItem($product->product_item_price, $product->product_quantity, $product->product_name, $pmconfigs['item_tax']);
        }

        if ($order->order_shipping > 0) {
            $merchant_helper->addItem($order->order_shipping, 1, "Доставка", $pmconfigs['delivery_tax']);
        }

        return $merchant_helper->generateMerchantReceipt();
    }

    function getUrlParams($pmconfigs) {
        $params = array();
        $params['order_id'] = JRequest::getInt("orderId");
        $params['hash'] = JRequest::getString("hash");
        $params['checkHash'] = 0;
        $params['checkReturnParams'] = 0;
        return $params;
    }

    function gesLastIMTransactionPaymentStatusData($order) {
        $transactions = $order->getListTransactions();

        foreach ($transactions as $transaction) {
            if (count($transaction->data) > 0) {
                foreach ($transaction->data as $data) {
                    if (isset($data->key) && $data->key == "statusIM") {
                        return $data;
                    }
                }
            }
        }

        return false;
    }

    private function convertAmount($amount) {
        return number_format(floatval($amount), 2, '.', '');
    }

    function getStatusFromResCode($rescode, $pmconfigs) {
        $status = 0;
        $statuses = array(
            3 => $pmconfigs['transaction_pending_status'],
            4 => $pmconfigs['transaction_canceled_status'],
            5 => $pmconfigs['transaction_end_status'],
            6 => $pmconfigs['transaction_hold_status'],
            7 => $pmconfigs['transaction_partialPaid_status'],
            8 => $pmconfigs['transaction_revert_status']
        );

        if (isset($statuses[$rescode])) {
            $status = $statuses[$rescode];
        }

        return $status;
    }

}
?>