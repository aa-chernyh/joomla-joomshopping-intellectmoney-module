<?php
/*
 * Joomla JoomShopping
 * Модуль для подключения системы IntellectMoney
 *
  Last Changed Date: 2019-01-15 09:00:00
 */
?>
<?php
$taxes = array(
    1 => "ставка НДС 20%",
    2 => "ставка НДС 10%",
    3 => "ставка НДС расч. 20/120",
    4 => "ставка НДС расч. 10/110",
    5 => "ставка НДС 0%",
    6 => "НДС не облагается"
);
?>
<div class="col100">
    <fieldset class="adminform">
        <h2>Основные настройки</h2>
        <table class="admintable" width = "100%" >
            <tr>
                <td  class="key">
                    Номер магазина продавца
                </td>
                <td>
                    <input type = "text" class = "inputbox" name = "pm_params[eshop_id]" size="45" value = "<?php echo $params['eshop_id'] ?>" />
                    <?php echo JHTML::tooltip('Номер магазина продавца можно узнать в личном кабинете пользователя на сайте https://intellectmoney.ru'); ?>
                </td>
            </tr>
            <tr>
                <td  class="key">
                    Секретное слово
                </td>
                <td>
                    <input type = "text" class = "inputbox" name = "pm_params[secret_key]" size="45" value = "<?php echo $params['secret_key'] ?>" />
                    <?php echo JHTML::tooltip('Секретное слово указывается вами в личном кабинете пользователя на сайте https://intellectmoney.ru'); ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Срок жизни счета
                </td>
                <td>
                    <input type = "text" class = "inputbox" name = "pm_params[expire_date]" size="45" value = "<?php echo $params['expire_date'] ?>" />
                    <?php echo JHTML::tooltip('Отвечает за срок жизни счёта. Если в течении срока жизни счёта он не будет оплачен - счёт атоматически отменяется. Указывать в часах. Максимальное значение 180 дней (4319 часов) установлено по умолчанию.'); ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    <?php echo _JSHOP_TRANSACTION_END; ?>
                </td>
                <td>
                    <?php
                    echo JHTML::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_end_status]', 'class = "inputbox" size = "1"', 'status_id', 'name', $params['transaction_end_status']);
                    echo JHTML::tooltip('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney полностью оплачен');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    <?php echo _JSHOP_TRANSACTION_PENDING; ?>
                </td>
                <td>
                    <?php
                    echo JHTML::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_pending_status]', 'class = "inputbox" size = "1"', 'status_id', 'name', $params['transaction_pending_status']);
                    echo JHTML::tooltip('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney создан');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Статус заказа для частично оплаченных транзакций
                </td>
                <td>
                    <?php
                    echo JHTML::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_partialPaid_status]', 'class = "inputbox" size = "1"', 'status_id', 'name', $params['transaction_partialPaid_status']);
                    echo JHTML::tooltip('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney частично оплачен');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Статус заказа для отменённых транзакций
                </td>
                <td>
                    <?php
                    echo JHTML::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_canceled_status]', 'class = "inputbox" size = "1"', 'status_id', 'name', $params['transaction_canceled_status']);
                    echo JHTML::tooltip('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney отменен');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Статус заказа для захолдированных транзакций
                </td>
                <td>
                    <?php
                    echo JHTML::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_hold_status]', 'class = "inputbox" size = "1"', 'status_id', 'name', $params['transaction_hold_status']);
                    echo JHTML::tooltip('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney захолдирован');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Статус заказа для возвращенных транзакций
                </td>
                <td>
                    <?php
                    echo JHTML::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_revert_status]', 'class = "inputbox" size = "1"', 'status_id', 'name', $params['transaction_revert_status']);
                    echo JHTML::tooltip('Укажите в какой статус переводить заказ, когда денежные средства со счёт в системе IntellectMoney, возвращены покупателю');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Url для уведомления
                </td>
                <td>
                    <?php
                    echo JURI::root() . "index.php?option=com_jshopping&controller=checkout&task=step7&act=notify&js_paymentclass=pm_im&no_lang=1";
                    echo JHTML::tooltip('Url для уведомления указывается вами в личном кабинете пользователя на сайте https://intellectmoney.ru (параметр URL Inform).');
                    ?>
                </td>
            </tr>
        </table>
        <h2>Онлайн касса</h2>
        <table class="admintable" width = "100%" >
            <tr>
                <td class="key">
                    Ставка НДС для товаров
                </td>
                <td>
                    <?php echo JHTML::_('select.genericlist', $taxes, 'pm_params[item_tax]', 'class = "inputbox" size = "1"', 'itemTax', 'name', $params['item_tax']);
                    echo JHTML::tooltip('Ставка налогооблажения для товара, число от 1 до 6');
                    ?>

                </td>
            </tr>
            <tr>
                <td class="key">
                    Ставка НДС для доставки
                </td>
                <td>
                    <?php echo JHTML::_('select.genericlist', $taxes, 'pm_params[delivery_tax]', 'class = "inputbox" size = "1"', 'deliveryTax', 'name', $params['delivery_tax']);
                    echo JHTML::tooltip('Ставка налогооблажения для доставки, число от 1 до 6');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Группа устройств
                </td>
                <td>
                    <input type = "text" class = "inputbox" name = "pm_params[group]" size="45" value = "<?php echo $params['group'] ?>" />
                    <?php echo JHTML::tooltip('Группа устройств на стороне OrangeData, поумолчанию Main'); ?>
                </td>
            </tr>
        </table>
        <h2>Расширенные настройки</h2>
        <table class="admintable" width = "100%" >
            </tr>
            <tr>
                <td class="key">
                    Способы оплаты
                </td>
                <td>
                    <input type = "text" class = "inputbox" name = "pm_params[preference]" size="45" value = "<?php echo $params['preference'] ?>" />
                    <?php echo JHTML::tooltip("Способы оплаты, которые будут доступны для выбора при оплате покупки."); ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Тестовый режим
                </td>
                <td>
                    <?php
                    print JHTML::_('select.booleanlist', 'pm_params[is_test]', 'class = "inputbox"', $params['is_test']);
                    echo JHTML::tooltip('Счета выставляются в тестовой валюте');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Холдирование
                </td>
                <td>
                    <?php
                    print JHTML::_('select.booleanlist', 'pm_params[is_hold_mode]', 'class = "inputbox"', $params['is_hold_mode']);
                    echo JHTML::tooltip('Включите данный параметр, если хотите использовать режим холдирования');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="key">
                    Срок холдирования
                </td>
                <td>
                    <input type = "text" class = "inputbox" name = "pm_params[hold_time]" size="45" value = "<?php echo $params['hold_time'] ?>" />
                    <?php echo JHTML::tooltip("При включенном параметре 'Режим холдирования' отвечате за дату зачисления денежных средств. Указывать в часах. Максимальное значение 119 часов по умолчанию 72 часа."); ?>
                </td>
            </tr>
        </table>
    </fieldset>
</div>
<div class="clr"></div>