#Модуль оплаты платежной системы IntellectMoney для CMS Joomla с модулем Joomshopping

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557765#whatnew.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557765#557765662ef90390d543de94aeaf7e67df99bc
<br>
Ответы на частые вопросы можно найти здесь: https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557765#5577651af1531eb252412b814b47ac45e3efcf
